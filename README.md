Repozytorium zawiera sprawozdanie oraz skrypt, które stanowią rozwiązanie problemu identyfikacji dwóch zbiorów danych.
Pierwszy zbiór stanowią dane statyczne, drugi zawiera dane dynamiczne.

## Opis plików:
1. Zad1 - skrypt, który dzieli dane statyczne na zbiór uczący oraz weryfikujący, oraz wizualizuję poszczególne zbiory.
2. Zad2 - skrypt identyfikujący dane dynamiczne, modelami różnego rzędu dynamiki, następnie przedstawia błędy oraz porównuje modele na wykresach.
3. Zad2nielin - skrypt dopasowujący modele różnego rzędu dynamiki nieliniowe, do danych dynamicznych. Przedstawia również błędy oraz porównuje modele na wykresach.
4. dyn_to_stat - skrypt, który na podstawie modelu dynamicznego uzyskuje charakterystykę statyczną
5. nonliear - skrypt identyfikujący dane statyczne za pomocą modeli nieliniowych.

Więcej informacji znajduję się w sprawozdaniu.