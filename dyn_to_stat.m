

wektoru=linspace(-1,1,50);
for i=1:length(wektoru)
    u=wektoru(i);
    x0=[0];
    wektory(i)=fsolve(@(y)fun(y,u, w_mod), -50);
end

figure;
plot(wektoru, wektory)
xlabel('u')
ylabel('y(u)')
grid on;
% title('Charakterystyka statyczna')
print(sprintf('Char_stat_dyn'),'-dpng','-r400');

function F = fun(y,u,w)
%     F = u*w(1)+u^2*w(2)+u^3*w(3)+u^4*w(4)+ y*w(5)+y^2*w(6)+y^3*w(7)+y^4*w(8);
    F = (w(1)+w(5)+w(9))*u + (w(2)+w(6)+w(10))*u^2 ;
    F = F + (w(3)+w(7)+w(11))*u^3 + (w(4)+w(8)+w(12)) * u^4;
    F = F + (w(13)+w(17)+w(21))*y + (w(14)+w(18)+w(22))*y^2;
    F = F + (w(15)+w(19)+w(23))*y^3 + (w(16)+w(20)+w(24))*y^2;
%     w_mod = [0.45
%           0.05
%          -0.46
%           0.78
%           0.95
%          -0.00
%           0.00
%          -0.00];
%     F=y^2;
    
end