stopien = 5;
M = ones(size,1+stopien);
E = [(0:stopien)' zeros(stopien+1,2)];
Mwer = ones(size,1+stopien);
for k=1:stopien+1
    M(:,k+1) = u.^k;
    Mwer(:,k+1) = uwer.^k;
end

M_plot = ones(size,1+stopien);
for k=1:stopien
    w = M(:,1:k+1)\y;
    
    y_werpred = Mwer(:,1:k+1) * w; 
    y_uczpred = M(:,1:k+1) * w;
    Ewer = sum((y_werpred-ywer).^2);
    Eucz = sum((y_uczpred-y).^2);
    E(k, 2) = Ewer;
    E(k, 3) = Eucz;
    
%     chstat(w, k);
    modlPor(y, y_uczpred,k)
end

%charakterystyka statyczna
function [] = chstat(w,k)
    figure;
    x = linspace(-1,1);
    M = ones(length(x), k+1);
    for i=1:k
        M(:,i+1) = (x').^i;
    end
    plot(x, M*w);
    grid on
    title(sprintf("Charekterystyka modelu stopnia %d", k))
%     title('Charakterystyka modelu, y(u)');
    xlabel('u');
    ylabel('y(u)')
%     print(sprintf('Char_st%d.png', k),'-dpng','-r400');
end

%prownanie modeli
function [] = modlPor(y, ypred,k)
    figure;
    plot(y);
    hold on;
    plot(ypred);
%     title(sprintf("Stopien %d", k))
    hold off;
    legend('y rzeczywiste', 'y modelu');
    xlabel('k');
    print(sprintf('Wyjscie_nlin_ucz%d.png', k),'-dpng','-r400');
end
