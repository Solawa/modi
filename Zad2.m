clear all;

load danedynucz23.txt;
load danedynwer23.txt;

% plot(danedynucz23);
u_ucz = danedynucz23(:,1);
y_ucz = danedynucz23(:,2);

u_wer = danedynucz23(:,1);
y_wer = danedynucz23(:,2);

dyn = 20;
E = [(1:dyn)' zeros(dyn,3)];
for i=1:dyn
    dlugosc = length(danedynucz23);
    y_wer_dyn = y_wer(i+1:dlugosc);
    y = y_ucz(i+1:dlugosc);
    
    M_wer = zeros(dlugosc-i, 2*i);
    M = zeros(dlugosc-i, 2*i);
    for k=1:i
        M(:,k+i) = y_ucz(i+1-k:dlugosc-k);
        M(:,k) = u_ucz(i+1-k:dlugosc-k);
        
        M_wer(:,k+i) = y_wer(i+1-k:dlugosc-k);
        M_wer(:,k) = y_wer(i+1-k:dlugosc-k);
    end
    w = M\y;
    ypred_wer = M_wer*w;
    ypred_ucz = M*w;
    
    
    %odpowiedz rekurencyjna
    ymod = zeros(dlugosc,1);
    for j=1:i
        ymod(j)=y_ucz(j);
    end
    M_rek = zeros(dlugosc-i, 2*i);
    for k=1:dlugosc-i
        for j=1:i
            M_rek(k,j+i) = ymod(k+i-j);
            M_rek(k,j) = u_ucz(k+i-j);
        end
        ymod(k+i) = M_rek(k, :) * w;
    end
    
    E(i,2) = sum((ypred_wer-y_wer_dyn).^2);
    E(i,3) = sum((ypred_ucz-y).^2);
    E(i,4) = sum((y_ucz-ymod).^2);
    
    modlPor(y_ucz, ymod, i)
    plot(u_ucz, 'b--');
end
E
% plot(ypred)
% hold on;
% plot(y_wer_dyn)
% hold off;

function [] = modlPor(y, ypred,k)
    figure;
    plot(y);
    hold on;
    plot(ypred);
    title(sprintf("Stopien %d", k-1));
%     hold off;
    legend('y prawdziwe', 'y pred');
end
