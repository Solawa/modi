clear all;
format bank;

load danedynucz23.txt;
load danedynwer23.txt;

% figure;
% plot(danedynucz23);
% title("Dane uczące")
% legend('u', 'y')
% print('Dane_ucz_Dyn.png','-dpng','-r400');
% figure;
% plot(danedynwer23);
% title("Dane weryfikujące")
% legend('u', 'y')
% print('Dane_wer_Dyn.png','-dpng','-r400');


u_ucz = danedynucz23(:,1);
y_ucz = danedynucz23(:,2);

u_wer = danedynwer23(:,1);
y_wer = danedynwer23(:,2);

dynamiki = 3;
stopnie = 4;
E_ucz_brek = zeros(dynamiki,stopnie);
E_wer_brek = zeros(dynamiki,stopnie);
E_ucz_rek = zeros(dynamiki,stopnie);
E_wer_rek = zeros(dynamiki,stopnie);
for dyn=1:dynamiki
    for st=1:stopnie
        dlugosc = length(danedynucz23);
        y_wer_dyn = y_wer(dyn+1:dlugosc);
        y_ucz_dyn = y_ucz(dyn+1:dlugosc);

        M_wer = zeros(dlugosc-dyn, 2*dyn*st);
        M = zeros(dlugosc-dyn, 2*dyn*st);
        for i_dyn=1:dyn
            for i_st=1:st
                M(:,st*(i_dyn-1)+i_st+dyn*st) = y_ucz(dyn+1-i_dyn:dlugosc-i_dyn).^(i_st);
                M(:,st*(i_dyn-1)+i_st) = u_ucz(dyn+1-i_dyn:dlugosc-i_dyn).^(i_st);

                M_wer(:,st*(i_dyn-1)+i_st+dyn*st) = y_wer(dyn+1-i_dyn:dlugosc-i_dyn).^(i_st);
                M_wer(:,st*(i_dyn-1)+i_st) = u_wer(dyn+1-i_dyn:dlugosc-i_dyn).^(i_st);
            end
        end
        w = M\y_ucz_dyn;
        
        ypred_wer = M_wer*w;
        ypred_ucz = M*w;
        ymod_wer = y_mod_rek_wer(y_wer, u_wer, dyn, st, w);
        ymod_ucz = y_mod_rek_wer(y_ucz, u_ucz, dyn, st, w);

        E_ucz_brek(dyn,st) = sum((ypred_ucz-y_ucz_dyn).^2);
        E_wer_brek(dyn,st) = sum((ypred_wer-y_wer_dyn).^2);
        E_ucz_rek(dyn,st) = sum((ymod_ucz-y_ucz).^2);
        E_wer_rek(dyn,st) = sum((ymod_wer-y_wer).^2);

%         modlPor(y_ucz, ypred_ucz, ymod_ucz, st, dyn)
%         modlPor(y_wer, ypred_wer, ymod_wer, st, dyn)
%         plot(u_ucz, 'b--')
        if(dyn==3 && st==4)
            w_mod = w;
        end
    end
end
E_ucz_brek = make_table(E_ucz_brek)
E_wer_brek = make_table(E_wer_brek)
E_ucz_rek = make_table(E_ucz_rek)
E_wer_rek = make_table(E_wer_rek)

function [] = modlPor(y, ypred, yrek, stopien, dynamika)
    figure;
    plot(y, '--');
    hold on;
    plot(ypred);
    plot(yrek);
%     title(sprintf("Stopien %d, dynamika %d", stopien, dynamika));
%     hold off;
    legend('y prawdziwe', 'y modelu bez rekurencji', 'y modelu z rekurencją');
%     print(sprintf("Ucz_NLIN_St%dDyn%d.png", stopien, dynamika),'-dpng','-r400');
end

function [ymod] = y_mod_rek_wer(y_ucz, u_ucz, dyn, st, w)
    dlugosc = length(y_ucz);
    ymod = zeros(dlugosc,1);
    for j=1:dyn
        ymod(j)=y_ucz(j);
    end
    M_rek = zeros(dlugosc-dyn, 2*dyn*st);
    for k=1:dlugosc-dyn
        for i_dyn=1:dyn
            for i_st=1:st
                M_rek(k,st*(i_dyn-1)+i_st+dyn*st) = ymod(k+dyn-i_dyn).^(i_st);
                M_rek(k,st*(i_dyn-1)+i_st) = u_ucz(k+dyn-i_dyn).^(i_st);
            end
        end
        ymod(k+dyn) = M_rek(k, :) * w;
    end
end

function [E_wyn] = make_table(E)
    [dyn, st] = size(E);
    E_wyn=[0 1:st;
    (1:dyn)' E];
end
