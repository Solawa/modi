clear all;
load danestat23.txt;

size_all = length(danestat23);
daneucz = danestat23(1:size_all/2,:);
danewer = danestat23(1+size_all/2:size_all,:);
% figure;
% plot(daneucz);
% figure;
% plot(daneucz);
u = daneucz(:,1);
y = daneucz(:,2);
size = length(u);
M = [ones(size,1) u];
w = M\y;

%dane uczace wizualizacja
plot(daneucz);
title("Dane uczące")
legend('u', 'y')
% print('Dane_ucz.png','-dpng','-r400');

%dane weryfikujace wizualizacja
plot(danewer);
title("Dane weryfikujące")
legend('u', 'y')
% print('Dane_wer.png','-dpng','-r400');

uwer = danewer(:,1);
ywer = danewer(:,2);
Mwer = [ones(size,1) uwer];
ypred = Mwer * w;
Ewer = sum((ypred-ywer).^2)
% plot(ypred)
% hold on;
% plot(ywer);
% legend('Pred', 'Wer')
% hold off;
